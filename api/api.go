package api

import "github.com/crackcomm/convey-actions/executor"

type ApiHandler struct {
	Executor *executor.Executor
}
